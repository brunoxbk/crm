from django import forms
from django.utils.translation import ugettext_lazy as _

class FormSearch(forms.Form):
    search = forms.CharField(
        label=_('Search'), max_length=120, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control' }))

class FormGroup(forms.Form):
    name = forms.CharField(
        label=_('Name'), max_length=120, required=True,
        widget=forms.TextInput(attrs={'class': 'form-control', 'required': True }))
