from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.contrib.auth.models import Group
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from apps.groups.forms import FormGroup, FormSearch
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _, activate
import datetime


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class List(LoginRequiredMixin, ListView):
    template_name = "groups/list.html"
    model = Group
    context_object_name = 'groups'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        query = Q()

        if self.request.GET.get('search', None):
            query.add(Q(name__icontains=self.request.GET['search']), Q.AND)

        return Group.objects.filter(query)

    def get_context_data(self, **kwargs):
        context = super(List, self).get_context_data(**kwargs)
        context.update(dict(form_search=FormSearch(self.request.GET)))
        return context

class New(CreateView):
    template_name = "groups/form.html"
    success_url = reverse_lazy('groups:list')
    model = Group
    form = FormGroup
    fields = ['name']

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        msg ="%s %s %s" % (
            _("Group"), self.object.name, _("added successfully!"))
        return super().form_valid(form)


class Edit(UpdateView):
    template_name = "groups/form.html"
    success_url = reverse_lazy('groups:list')
    model = Group
    form = FormGroup
    fields = ['name']

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        msg ="%s %s %s" % (
            _("Group"), self.object.name, _("updated successfully!"))
        return super().form_valid(form)


class Delete(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('codes:list')
    template_name = 'groups/delete.html'
    success_message = _('Group deleted successfully')
    slug_field = 'pk'
