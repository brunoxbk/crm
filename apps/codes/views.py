from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from apps.codes.forms import FormCode, FormSearch
from apps.archives.models import Code
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
from django.utils.translation import activate
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import user_passes_test
import datetime


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class List(LoginRequiredMixin, ListView):
    template_name = "codes/list.html"
    model = Code
    context_object_name = 'codes'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('search', None):
            query.add(Q(code__icontains=self.request.GET['search']), Q.AND)

        return Code.objects.filter(query)

    def get_context_data(self, **kwargs):
        context = super(List, self).get_context_data(**kwargs)
        context.update(dict(form_search=FormSearch(self.request.GET)))
        return context


class New(LoginRequiredMixin, FormView):
    template_name = 'codes/form.html'
    form_class = FormCode
    success_url = reverse_lazy('codes:list')

    def form_valid(self, form):
        data = form.cleaned_data

        code = Code(
            title=data['title'],
            code=data['code'],
            validity=data['validity'],
        )

        code.save()

        msg ="%s %s %s" % (
            _("Archive"), code.title, _("added successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(New, self).form_valid(form)


class Edit(LoginRequiredMixin, FormView):
    template_name = "codes/form.html"
    form_class = FormCode
    success_url = reverse_lazy('codes:list')

    def get_object(self):
        return get_object_or_404(Code, id=self.kwargs.get('pk'))

    def get_initial(self):
        self.initial = model_to_dict(self.get_object())
        return self.initial.copy()

    def form_valid(self, form):
        data = form.cleaned_data
        code = self.get_object()

        code.title=data['title']
        code.code=data['code']
        code.validity=data['validity']

        code.save()

        msg = "%s alterado com sucesso." % code.title
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(Edit, self).form_valid(form)


class Detail(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Code
    context_object_name = 'code'
    template_name = "codes/detail.html"


class Delete(LoginRequiredMixin, DeleteView):
    model = Code
    success_url = reverse_lazy('codes:list')
    template_name = 'codes/delete.html'
    success_message = _('Code deleted successfully')
    slug_field = 'pk'
