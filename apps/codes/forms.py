from django import forms
from django.conf import settings
from datetime import datetime
from apps.archives.models import Code
from django.utils.translation import ugettext_lazy as _
import re
from django.core.exceptions import ValidationError

class FormSearch(forms.Form):
    search = forms.CharField(
        label=_('Search'), max_length=120, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control' }))


class FormCode(forms.Form):
    title = forms.CharField(
        label=_('Title Archive'), max_length=120, required=True,
        widget=forms.TextInput(attrs={'class': 'form-control' }))
    code = forms.CharField(
        label=_('Code'), max_length=120, required=True,
        widget=forms.TextInput(attrs={'class': 'form-control' }))
    validity = forms.IntegerField(
        label=_('Validity'), required=True, initial=0,
        help_text=_('In years, 0 to permanent.'),
        widget=forms.NumberInput(attrs={'class': 'form-control' }))

    class Meta:
        fieldsets = (
          (None, {
            'fields': ('title',)
          }),
          (None, {
            'fields': ('code',)
          }),
          (None, {
            'fields': ('validity',)
          }),
        )
