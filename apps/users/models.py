from django.db import models
from django.contrib import auth
from django.contrib.auth.models import AbstractBaseUser, \
    _user_get_all_permissions, BaseUserManager, Group, \
    Permission, _user_has_perm, _user_has_module_perms, \
    PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
import re
from django.core import validators
from django.core.mail import send_mail


class UserManager(BaseUserManager):
    def _create_user(
        self, username, email, password, is_staff,
            is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError(_('The given username must be set'))
        email = self.normalize_email(email)
        user = self.model(
            username=username, email=email,
            is_staff=is_staff, is_active=False,
            is_superuser=is_superuser, last_login=now,
            date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(
            username, email, password, False, False, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        user = self._create_user(
            username, email, password, True, True, **extra_fields)
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICE = (
        ('', '---------'),
        ('M', _('Male')),
        ('F', _('Female')),
    )

    username = models.CharField(
        _('username'), max_length=255, unique=True,
        help_text=_(
            'Required. 30 characters or fewer. Letters,'
            'numbers and @/./+/-/_ characters'),
        validators=[
            validators.RegexValidator(
                re.compile('^[\w.@+-]+$'),
                _('Enter a valid username.'), _('invalid'))
        ])
    first_name = models.CharField(
        _('First name'), max_length=255, blank=True)
    last_name = models.CharField(
        _('Last name'), max_length=255, blank=True)
    cpf = models.CharField(
        _('CPF'), max_length=255, blank=True, null=True)
    email = models.EmailField(
        _('Email address'), unique=True)
    gender = models.CharField(
        _('Gender'), max_length=2, choices=GENDER_CHOICE, blank=True, null=True)
    birth = models.DateField('Nascimento', null=True, blank=True)
    phone = models.CharField(
        _('Phone'), max_length=55, blank=True, null=True)
    hash_util = models.CharField(
        _('Hash'), max_length=255, blank=True, null=True)
    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_(
            'Designates whether the user'
            ' can log into this admin site.'))
    is_active = models.BooleanField(
        _('active'), default=False,
        help_text=_(
            'Designates whether this user should be treated as active.'
            ' Unselect this instead of deleting accounts.'))
    is_member = models.BooleanField('Membro', default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    description = models.TextField("Texto", null=True, blank=True)
    photo = models.ImageField(
        'Foto', upload_to='uploads/users/', null=True, blank=True)
    created = models.DateTimeField(
        _('created'), auto_now_add=True)
    changed = models.DateTimeField(
        _('changed'), auto_now=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    objects = UserManager()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        # app_label = "myauth"
    @property
    def full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def get_avatar(self):
        if self.avatar:
            return self.avatar.url
        else:
            return '/static/img/avatar.png'

    def get_group_permissions(self, obj=None):
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                if obj is not None:
                    permissions.update(backend.get_group_permissions(self,
                                                                     obj))
                else:
                    permissions.update(backend.get_group_permissions(self))
        return permissions

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj)

    def has_perm(self, perm, obj=None):
        # Ativo se o superusuario tiver todas as permissoes.
        if self.is_active and self.is_superuser:
            return True

        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)
