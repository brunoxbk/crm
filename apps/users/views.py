from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from apps.users.forms import FormCreate, FormEdit, \
    FormSearch, FormPass, FormProfile
from django.forms.models import model_to_dict
from apps.users.models import User
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _, activate
import datetime
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class LoginRequiredMixin2(object):
    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin2, self)\
            .dispatch(request, *args, **kwargs)


class New(LoginRequiredMixin2, FormView):
    template_name = 'users/form.html'
    form_class = FormCreate
    success_url = reverse_lazy('users:list')

    def form_valid(self, form):
        data = form.cleaned_data

        user = User(
            username=data['email'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            cpf=data['cpf'],
            email=data['email'],
            gender=data['gender'],
            birth=data['birth'],
            phone=data['phone'],
            is_superuser=data['is_superuser'],
            is_active=data['is_active']
        )

        user.set_password(data['password_confirm'])

        user.save()

        msg = "%s %s %s" % (
            _("User"), user.get_short_name(), _("added successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(New, self).form_valid(form)


class Edit(LoginRequiredMixin2, FormView):
    template_name = "users/form.html"
    form_class = FormEdit
    success_url = reverse_lazy('users:list')

    def get_object(self):
        return get_object_or_404(User, id=self.kwargs.get('pk'))

    def get_initial(self):
        user = self.get_object()
        initial = model_to_dict(user)
        if user.birth:
            initial['birth'] = user.birth.strftime('%d/%m/%Y')
        return initial.copy()

    def form_valid(self, form):
        data = form.cleaned_data
        user = self.get_object()

        user.first_name = data['first_name']
        user.last_name = data['last_name']

        if user.cpf != data['cpf']:
            user.cpf = data['cpf']

        if user.email != data['email']:
            user.email = data['email']
            user.username = data['email']

        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.gender = data['gender']
        user.birth = data['birth']
        user.phone = data['phone']
        user.is_superuser = data['is_superuser']
        user.is_active = data['is_active']

        user.save()

        msg = "%s %s %s" % (
            _("User"), user.get_short_name(), _("updated successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(Edit, self).form_valid(form)


class EditProfile(LoginRequiredMixin, FormView):
    template_name = "users/form_profile.html"
    form_class = FormProfile
    success_url = reverse_lazy('core:home')

    def get_object(self):
        return get_object_or_404(User, id=self.kwargs.get('pk'))

    def get_initial(self):
        initial = model_to_dict(self.request.user)
        if self.request.user.birth:
            initial['birth'] = self.request.user.birth.strftime('%d/%m/%Y')
        return initial.copy()

    def form_valid(self, form):
        data = form.cleaned_data
        user = self.request.user

        user.first_name = data['first_name']
        user.last_name = data['last_name']

        if user.cpf != data['cpf']:
            user.cpf = data['cpf']

        if user.email != data['email']:
            user.email = data['email']
            user.username = data['email']

        user.first_name = data['first_name']
        user.last_name = data['last_name']
        user.gender = data['gender']
        user.birth = data['birth']
        user.phone = data['phone']

        user.save()

        msg = "%s %s %s" % (
            _("User"), user.get_short_name(), _("updated successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(EditProfile, self).form_valid(form)


class EditPass(LoginRequiredMixin, FormView):
    template_name = "users/form_pass.html"
    form_class = FormPass
    success_url = reverse_lazy('core:home')

    def form_valid(self, form):
        data = form.cleaned_data
        user = self.request.user

        user.set_password(data['password_confirm'])

        user.save()

        msg = "%s %s %s" % (
            _("User"), user.get_short_name(),
            _("password changed successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(EditPass, self).form_valid(form)


class List(LoginRequiredMixin2, ListView):
    template_name = "users/list.html"
    model = User
    context_object_name = 'users'
    paginate_by = 25

    def get_queryset(self, **kwargs):
        query = Q(is_active=True)

        if self.request.GET.get('search', None):
            query.add(
                Q(first_name__icontains=self.request.GET['search']) |
                Q(last_name__icontains=self.request.GET['search']) |
                Q(email__icontains=self.request.GET['search']), Q.AND)

        return User.objects.filter(query).order_by('-created')

    def get_context_data(self, **kwargs):
        context = super(List, self).get_context_data(**kwargs)
        context.update(dict(form_search=FormSearch(self.request.GET)))
        return context


class Detail(LoginRequiredMixin2, DetailView):
    slug_field = 'id'
    model = User
    context_object_name = 'user'
    template_name = "users/detail.html"


class Delete(LoginRequiredMixin2, DeleteView):
    model = User
    success_url = reverse_lazy('users:list')
    template_name = 'users/delete.html'
    success_message = _("User added successfully!")
    slug_field = 'pk'


class ChangePass(LoginRequiredMixin2, FormView):
    template_name = 'users/form_pass.html'
    form_class = FormPass
    success_url = reverse_lazy('users:list')

    def get_object(self):
        return get_object_or_404(User, id=self.kwargs.get('pk'))

    def form_valid(self, form):
        data = form.cleaned_data
        user = self.get_object()

        user.set_password(data['password_confirm'])

        user.save()

        msg = "%s %s %s" % (
            _("User"), user.get_short_name(),
            _("password changed successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(ChangePass, self).form_valid(form)
