from django import forms
from django.conf import settings
from apps.users.models import User
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
import re
from django.core.exceptions import ValidationError


class FormSearch(forms.Form):
    search = forms.CharField(
        label=_('Search'), max_length=120, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control' }))


class FormCreate(forms.Form):
    email = forms.EmailField(
        label=_('Email'), max_length=80, required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off',
                }))
    first_name = forms.CharField(
        label=_('First Name'), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={
            'class': 'form-control',
            'required': True,
            }))
    last_name = forms.CharField(
        label=_("Last Name"), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'required': True,
                }))
    is_active = forms.BooleanField(
        label=_('Active'), required=False, initial=True,
        widget=forms.CheckboxInput(
            attrs={
                'required': False,
                'class': 'filled-in chk-col-indigo'
                }))
    is_superuser = forms.BooleanField(
        label=_('Superuser'), required=False,
        widget=forms.CheckboxInput(
            attrs={
                'required': False,
                'class': 'filled-in chk-col-indigo'
                }))
    phone = forms.CharField(
        label=_('Phone'), max_length=80, required=False,
        widget=forms.TextInput(
            attrs={
                'required': False,
                'class': 'form-control',
                }))
    password = forms.CharField(
        label=_('Password'), max_length=80, required=True,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off'}))
    password_confirm = forms.CharField(
        label=_('Confirm'), max_length=80, required=True,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off'}))
    birth = forms.DateField(
        label='Nascimento', required=False,
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(attrs={'class': 'form-control', 'required': True}))
    cpf = forms.CharField(
        label=_('CPF'), required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'required': False,
                'autocomplete': 'off',
                }))
    gender = forms.ChoiceField(
        label=_('Gender'),
        choices=User.GENDER_CHOICE, required=False,
        widget=forms.Select(attrs={
            'class': 'form-control show-tick',
            'required': True, }))

    group = forms.ModelChoiceField(
        label=_('Group'),  required=False,
        queryset=Group.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control show-tick'}))

    def clean_phone(self):
        data = self.cleaned_data['phone']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean_password_confirm(self):
        data = self.cleaned_data

        if data["password"] != data['password_confirm']:
            raise ValidationError(_("Password does not match"))

        return data['password_confirm']


    def clean_email(self):
        data = self.cleaned_data['email']

        try:
            user = User.objects.get(email=data)
        except Exception as e:
            user = None

        if not user:
            return data
        else:
            raise ValidationError(_("E-mail already registered"))

    class Meta:
        fieldsets = (
          (_('Personal Data'), {
            'fields': (
                'first_name', 'last_name', 'gender', 'birth', 'cpf') }),
          (_('Contact'), {
            'fields': ('is_active',)
          }),
          (_('Auth'), {
            'fields': ('is_superuser', 'group')
          }),
          (_('Contact'), {
            'fields': ('phone', 'email',)
          }),
          (_('Accsess Data'), {
            'fields': ('password', 'password_confirm')
          }),
        )


class FormEdit(forms.Form):
    email = forms.EmailField(
        label=_('Email'), max_length=80, required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off',
                }))
    first_name = forms.CharField(
        label=_('First Name'), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={
            'class': 'form-control',
            'required': True,
            }))
    last_name = forms.CharField(
        label=_("Last Name"), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'required': True,
                }))
    is_active = forms.BooleanField(
        label=_('Active'), required=False, initial=True,
        widget=forms.CheckboxInput(
            attrs={
                'required': False,
                'class': 'filled-in chk-col-indigo'
                }))
    is_superuser = forms.BooleanField(
        label=_('Superuser'), required=False,
        widget=forms.CheckboxInput(
            attrs={
                'required': False,
                'class': 'filled-in chk-col-indigo'
                }))
    phone = forms.CharField(
        label=_('Phone'), max_length=80, required=False,
        widget=forms.TextInput(
            attrs={
                'required': False,
                'class': 'form-control',
                }))

    birth = forms.DateField(
        label=_('Birth'), required=False,
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={'class': 'form-control', 'required': False}))
    cpf = forms.CharField(
        label=_('CPF'), required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'required': False,
                'autocomplete': 'off',
                }))
    gender = forms.ChoiceField(
        label=_('Gender'),
        choices=User.GENDER_CHOICE, required=False,
        widget=forms.Select(attrs={
            'class': 'form-control show-tick',
            'required': True, }))

    group = forms.ModelChoiceField(
        label=_('Group'),
        queryset=Group.objects.all(),  required=False,
        widget=forms.Select(attrs={'class': 'form-control show-tick'}))

    id = forms.CharField(
        label='', required=True, widget=forms.HiddenInput)

    def clean_cpf(self):
        data = self.cleaned_data['cpf']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean_phone(self):
        data = self.cleaned_data['phone']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean(self):
        data = self.cleaned_data

        users = User.objects.filter(cpf=data['email']).exclude(id=data['id'])

        if users:
            raise ValidationError({'email': ["Email já em utilização", ]})

    class Meta:
        fieldsets = (
          (_('Personal Data'), {'fields': (
            'first_name', 'last_name', 'gender', 'birth', 'cpf'
            )}),
          (None, {'fields': ('is_active',)}),
          (None, {'fields': ('is_superuser',)}),
          (_('Contact'), {'fields': ('phone', 'email',)
          }),
          (None, {'fields': ('group',)}),
          (None, {'fields': ('id',)}),
        )


class FormProfile(forms.Form):
    email = forms.EmailField(
        label=_('Email'), max_length=80, required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off',
                }))
    first_name = forms.CharField(
        label=_('First Name'), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'required': True}))
    last_name = forms.CharField(
        label=_("Last Name"), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'required': True}))
    phone = forms.CharField(
        label=_('Phone'), max_length=80, required=False,
        widget=forms.TextInput(
            attrs={'required': False, 'class': 'form-control'}))

    birth = forms.DateField(
        label=_('Birth'), required=False,
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={'class': 'form-control', 'required': False}))
    cpf = forms.CharField(
        label=_('CPF'), required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'required': False,
                'autocomplete': 'off',
                }))
    gender = forms.ChoiceField(
        label=_('Gender'),
        choices=User.GENDER_CHOICE, required=False,
        widget=forms.Select(attrs={
            'class': 'form-control show-tick',
            'required': True, }))

    id = forms.CharField(
        label='', required=True, widget=forms.HiddenInput)

    def clean_cpf(self):
        data = self.cleaned_data['cpf']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean_phone(self):
        data = self.cleaned_data['phone']
        return re.sub('[^A-Za-z0-9]+', '', data)

    def clean(self):
        data = self.cleaned_data

        users = User.objects.filter(cpf=data['email']).exclude(id=data['id'])

        if users:
            raise ValidationError({'email': ["Email já em utilização", ]})

    class Meta:
        fieldsets = (
          (_('Personal Data'), {'fields': (
            'first_name', 'last_name', 'gender', 'birth', 'cpf'
            )}),
          (_('Contact'), {'fields': ('phone', 'email',)}),
          (None, {'fields': ('id',)}),
        )

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_('Password Confirmation'), widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Senhas não conferem")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()
    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all().order_by('name'),
        widget=forms.CheckboxSelectMultiple(), required=False, label='Grupos')

    def __init__(self, *args, **kargs):
        super(UserChangeForm, self).__init__(*args, **kargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            # self.fields['created'].widget.attrs['readonly'] = True
            # self.fields['last_login'].widget.attrs['readonly'] = True
            self.fields['password'].help_text = (
                _("To change the user password enter ") + \
                "<a href=\'/admin/users/user/%d/password/'>" + \
                _(" this form</a>.") % instance.pk)

    class Meta:
        model = User
        fields = (
            'username', 'email', 'password',
            'is_active', 'is_member', 'is_superuser')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class FormPass(forms.Form):
    password = forms.CharField(
        label=_('Password'), max_length=80, required=True,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off'}))
    password_confirm = forms.CharField(
        label=_('Confirm'), max_length=80, required=True,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'required': True,
                'autocomplete': 'off'}))

    def clean_password_confirm(self):
        data = self.cleaned_data

        if data["password"] != data['password_confirm']:
            raise ValidationError(_("Password does not match"))

        return data['password_confirm']

    class Meta:
        fieldsets = (
          (None, {
            'fields': ('password',)
          }),
          (None, {
            'fields': ('password_confirm',)
          }),
        )
