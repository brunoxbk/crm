from django.contrib import admin
from apps.users.models import User
from apps.users.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.admin import UserAdmin


class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances
    actions = None
    form = UserChangeForm
    add_form = UserCreationForm
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'is_active', 'is_member',)
    list_filter = ('is_member', 'groups')
    list_editable = ('is_active',)
    fieldsets = (
        ('Authorization', {
            'fields': ('username', 'email', 'password')}),
        ('Pessoal', {
            'fields': (
                'first_name', 'last_name', 'photo', 'city', 'province')}),
        ('Permissoes',
            {'fields': (
                'is_active', 'is_member', 'is_staff')}),
        ('Grupo', {'fields': ('groups',)}))

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        ('Authorization', {
            'fields': ('username', 'email', 'password1', 'password2')}),
        ('Pessoal', {
            'fields': ('first_name', 'last_name', 'photo', )}),
        ('Permissoes',
            {'fields': ('is_active', 'is_member', 'is_staff')}),
        ('Grupo', {'fields': ('groups',)})
        #  (None, {
        #     'classes': ('wide',),
        #    'fields': ('username', 'email', 'password1', 'password2')}
        # ),
    )
    search_fields = ('username', 'email')
    ordering = ('username',)
    filter_horizontal = ()

    # def get_queryset(self, request):
    #     queryset = super(CustomUserAdmin, self).get_queryset(request)
    #     q = queryset.filter()
    #     if not request.user.is_superuser:
    #         q = q.filter(site=request.user.site)
    #     return q

    # def save_model(self, request, obj, form, change):
    #     if not change:
    #         obj.creator = request.user
    #
    #     if not request.user.is_superuser:
    #         obj.site = request.user.site
    #     obj.save()


admin.site.register(User, CustomUserAdmin)
