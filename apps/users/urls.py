from django.urls import path
from apps.users import views

app_name = 'users'

urlpatterns = [
    path('', views.List.as_view(), name='list'),
    path('new/', views.New.as_view(), name='new'),
    path('profile/', views.EditProfile.as_view(), name='profile'),
    path('profile/password/', views.EditPass.as_view(), name='profile-pass'),
    path('edit/<int:pk>/', views.Edit.as_view(), name='edit'),
    path('detail/<int:pk>/', views.Detail.as_view(), name='detail'),
    path('change-pass/<int:pk>/', views.ChangePass.as_view(), name='change'),
    path('delete/<int:pk>/', views.Delete.as_view(), name='delete'),
]
