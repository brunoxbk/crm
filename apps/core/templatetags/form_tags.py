from django import template
from urllib.parse import urlencode

register = template.Library()


@register.simple_tag()
def get_label(field, form):
    if field != '__all__':
        label = form.fields[field].label
    else:
        label = 'Erro'
    return label


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)
    return urlencode(query)


@register.filter(name='split')
def split(value):
    return value.split('/')
