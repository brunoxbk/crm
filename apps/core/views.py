from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.contrib.auth.decorators import login_required
from apps.core.forms import FormCustomLogin, FormMirror
from django.http import HttpResponseRedirect
from django.utils.translation import activate
from apps.archives.models import Code
from apps.archives.models import Archive
from django.contrib.auth.models import Group
from apps.users.models import User
from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse_lazy
from urllib.parse import urlencode
import datetime


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "core/home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['codes'] = Code.objects.all().count()
        context['users'] = User.objects.all().count()
        context['archives'] = Archive.objects.all().count()
        context['groups'] = Group.objects.all().count()
        return context


class LoginView(FormView):
    success_url = '/'
    # form_class = AuthenticationForm
    form_class = FormCustomLogin
    # redirect_field_name = REDIRECT_FIELD_NAME
    template_name = "core/login.html"

    def form_valid(self, form):
        data = form.cleaned_data

        user = authenticate(
            self.request,
            username=data['username'],
            password=data['password'])

        if user is not None:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect('/')
            else:
                msg = 'Conta inativa'
                messages.add_message(self.request, messages.WARNING, msg)
                return HttpResponseRedirect('/login/')
        else:
            msg = 'Senha incorreta'
            messages.add_message(self.request, messages.WARNING, msg)
            return HttpResponseRedirect('/login/')
        return super(LoginView, self).form_valid(form)


class LogoutView(LoginRequiredMixin, RedirectView):
    url = '/login/'
    permanent = True

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class MirrorFormView(LoginRequiredMixin, FormView):
    template_name = "core/form_mirror.html"
    form_class = FormMirror

    def form_valid(self, form):
        data = form.cleaned_data

        # url = "http://stackoverflow.com/search?q=question"
        # params = {'lang':'en','tag':'python'}
        #
        # url_parts = list(urlparse.urlparse(url))
        query = urlencode(data, doseq=True)
        print(query)
        # query.update(params)
        #
        # url_parts[4] = urlencode(query)
        # reverse_lazy('codes:list')

        return render(
            self.request, 'core/mirror3.html', data,
            content_type="text/html")
