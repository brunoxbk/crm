# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
from apps.users.models import User
from apps.archives.models import Archive, Code
from datetime import datetime


class Command(BaseCommand):
    help = "Dados Códigos"
    can_import_settings = True

    def seed(self):

        try:

            CODES = [
                ("062.7", "062.7 - Parecer Juridico", 0),
                ("010.3", "010.3 - Pauta (Ata)", 5),
                ("055", "055 - Balancete", 0),
                ("057", "057 - Prestação de contas", 5),
                ("051.12", "051.12 - Orçamento (proposta)", 2),
                ("036.1", "036.1 - Licitação", 8),
                ("022.22", "022.22 - Contrato de estagiário", 2),
                ("029.11", "029.11 - Folha de pagamento", 53),
            ]

            for c in CODES:
                code = Code(code=c[0], title=c[1], validity=c[2])
                code.save()

        except Exception as e:
            print(e)

    def handle(self, *args, **options):
        self.seed()
