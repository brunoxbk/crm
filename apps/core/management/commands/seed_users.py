# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
from apps.users.models import User
from apps.archives.models import Archive, Code
from datetime import datetime


class Command(BaseCommand):
    help = "Dados Tabela"
    can_import_settings = True

    def seed_user(self):

        try:

            user = User(
                username='73585980325',
                first_name='Dráuzio',
                last_name='Varella',
                cpf='73585980325',
                email='drauzio@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='86988441567',
                is_superuser=True,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

            user = User(
                username='90561143870',
                first_name='Gergory',
                last_name='House',
                cpf='90561143870',
                email='house@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='86988888888',
                is_superuser=False,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

            user = User(
                username='74147974732',
                first_name='Robert',
                last_name='Rey',
                cpf='74147974732',
                email='rey@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='89998989997',
                is_superuser=False,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

            user = User(
                username='66508583310',
                first_name='Hans',
                last_name='Chucrutes',
                cpf='66508583310',
                email='hanschucrutes@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='89999059998',
                is_superuser=False,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

        except Exception as e:
            print(e)

    def handle(self, *args, **options):
        self.seed_user()
