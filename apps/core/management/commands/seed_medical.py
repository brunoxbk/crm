# coding: utf-8
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from django.conf import settings
from apps.archives.models import Code
from apps.archives.models import Archive
from apps.users.models import User
import os
from openpyxl import load_workbook


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.read()

    def read(self):
        path = os.path.join(settings.BASE_DIR, 'docs', 'medical.xlsx')
        wb = load_workbook(path)
        ws = wb.active
        max_row = ws.max_row
        cell_prat = None
        cell_linha = None
        cell_crm = None
        for i in range(1, max_row + 1):
            try:
                if ws.cell(row=i, column=1).value is not None and \
                        ws.cell(row=i, column=1).value != cell_prat:
                    cell_prat = ws.cell(row=i, column=1).value

                if ws.cell(row=i, column=2).value is not None and \
                        ws.cell(row=i, column=2).value != cell_linha:
                    cell_linha = ws.cell(row=i, column=2).value

                if ws.cell(row=i, column=3).value is not None and \
                        ws.cell(row=i, column=3).value != cell_crm:
                    cell_crm = ws.cell(row=i, column=3).value

                try:
                    archive = Archive.objects.get(keywords=cell_crm)
                except Archive.DoesNotExist:
                    archive = None

                if archive:
                    self.stdout.write(
                        self.style.SUCCESS(
                            "%s | %s | %s" % (
                                cell_prat.split(' ')[1],
                                cell_linha.split(' ')[1],
                                cell_crm)
                            )
                        )
                    self.stdout.write(
                        self.style.WARNING(
                            archive.description if archive is not None else ''
                        )
                    )
                    archive.shelf = cell_prat.split(' ')[1]
                    archive.row = cell_linha.split(' ')[1]
                    archive.save()
                    self.stdout.write(self.style.SUCCESS(80 * "-"))
            except Exception as e:
                self.stderr.write(self.style.ERROR(CommandError(e)))
