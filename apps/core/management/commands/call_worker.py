# -*- coding: utf-8 -*-
import os
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils import timezone
from subprocess import call
import logging


class Command(BaseCommand):
    help = "Command Worker"
    can_import_settings = True
    logdir = os.path.join(settings.BASE_DIR, 'logs')
    base = settings.BASE_DIR


    def add_arguments(self, parser):
        parser.add_argument('command', type=str)

    def start(self):
        call([
            "celery", "multi", "start", "-A", "wkmega",
            "worker", "-B", "-l", "info",
            "--logfile=" + self.logdir +"/%n%I.log",
            "--pidfile=" + self.base + "/%n.pid"
        ])

    def stop(self):
        call([
            "celery", "multi", "stopwait", "worker",
            "--pidfile=" + self.base + "/%n.pid"
        ])


    def handle(self, *args, **options):
        if options['command'] and options['command'] == "start":
            self.start()
        elif options['command'] and options['command'] == "stop":
            self.stop()
        elif options['command'] and options['command'] == "info":
            self.info()
        else:
            print("Especifique um comando")
