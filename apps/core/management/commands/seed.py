# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
from apps.users.models import User
from apps.archives.models import Archive, Code
from datetime import datetime


class Command(BaseCommand):
    help = "Dados Tabela"
    can_import_settings = True

    def seed_user(self):

        try:

            CODES = [
                ("062.7", "062.7 - Parecer Juridico", 0),
                ("010.3", "010.3 - Pauta (Ata)", 5),
                ("055", "055 - Balancete", 0),
                ("057", "057 - Prestação de contas", 5),
                ("051.12", "051.12 - Orçamento (proposta)", 2),
                ("036.1", "036.1 - Licitação", 8),
                ("022.22", "022.22 - Contrato de estagiário", 2),
                ("029.11", "029.11 - Folha de pagamento", 53),
            ]

            for c in CODES:
                code = Code(code=c[0], title=c[1], validity=c[2])
                code.save()

            user = User(
                username='73585980325',
                first_name='Dráuzio',
                last_name='Varella',
                cpf='73585980325',
                email='drauzio@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='86988441567',
                is_superuser=True,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 11',
                creator=user
            )

            archive.save()

            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 12',
                creator=user
            )

            archive.save()

            user = User(
                username='90561143870',
                first_name='Gergory',
                last_name='House',
                register='CRM1234',
                cpf='90561143870',
                email='house@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='86988888888',
                is_superuser=False,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 21',
                creator=user
            )

            archive.save()

            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 22',
                creator=user
            )

            archive.save()

            user = User(
                username='74147974732',
                first_name='Robert',
                last_name='Rey',
                register='CRM1111',
                cpf='74147974732',
                email='rey@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='89998989997',
                is_superuser=False,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()


            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 51',
                creator=user
            )

            archive.save()

            archive = Archive(
                reference='1234',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 52',
                creator=user
            )

            archive.save()


            user = User(
                username='66508583310',
                first_name='Hans',
                last_name='Chucrutes',
                register='CRM1122',
                cpf='66508583310',
                email='hanschucrutes@gmail.com',
                gender='M',
                birth=datetime.strptime('28/11/1991', '%d/%m/%Y').date(),
                phone='89999059998',
                is_superuser=False,
                is_active=True,
            )

            user.set_password('crmpass')
            user.save()

            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 61',
                creator=user
            )

            archive.save()

            archive = Archive(
                reference='',
                box='3',
                shelf='1',
                row='1',
                year='2016',
                description='Descrição arquivo 62',
                creator=user
            )

            archive.save()

        except Exception as e:
            print(e)

    def handle(self, *args, **options):
        self.seed_user()
