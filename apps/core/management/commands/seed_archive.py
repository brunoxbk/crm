# coding: utf-8
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from django.conf import settings
from apps.archives.models import Code
from apps.archives.models import Archive
from apps.users.models import User
import os
from openpyxl import load_workbook


class Command(BaseCommand):


    def handle(self, *args, **options):
        self.read()

    def read(self):
        path = os.path.join(settings.BASE_DIR, 'docs', 'medicos.xlsx')
        wb = load_workbook(path)

        code = Code.objects.get(code='950')
        creator = User.objects.first()

        ws = wb.active

        max_row = ws.max_row

        for i in range(1,max_row+1):
            try:
                cell_num = ws.cell(row=i,column=1)
                cell_nome = ws.cell(row=i,column=2)
                cell_esp = ws.cell(row=i,column=3)
                dados = "Dados Pessoais"
                description = """
                    %s
                    %s
                    %s""" % \
                    (cell_nome.value, cell_esp.value, dados)
                archive = Archive(
                    code=code,
                    shelf=True,
                    keywords=cell_num.value,
                    description=description,
                    creator=creator
                )
                archive.save()
                print(archive.__dict__)
                self.stdout.write(
                    self.style.SUCCESS(
                        "%s | %s - %s | %s" % \
                        (cell_num.value, cell_nome.value, cell_esp.value, dados)))
            except Exception as e:
                self.stderr.write(self.style.ERROR(CommandError(e)))


