# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
from apps.lottery.models import Ticket
from django.utils import timezone
import logging
import xml.etree.ElementTree as ET
import requests
from datetime import timedelta


class Command(BaseCommand):
    help = "Dados Tabela"
    can_import_settings = True

    def check_transaction(self, code):
        url = settings.TRANSACTIONS_PG
        # payload = {"email": settings.EMAIL_PAG, 'token': settings.TOKEN_PAG}
        # url_param = urllib.parse.urlencode(payload, doseq=True)
        # headers = {'Content-Type':  settings.CONTENT_TYPE_UOL}

        url = url + code + "?email=" + settings.EMAIL_PAG + \
            '&token=' + settings.TOKEN_PAG

        r = requests.get(url)

        try:
            print(r.text)
            root = ET.fromstring(r.text)
            status = root.find('status').text
        except Exception as e:
            print(e)
            status = '1'

        # print(status)
        return status

    def data_payment(self):

        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger(__name__)
        logger.info("init script")

        start_date = timezone.now() - timedelta(days=45)
        end_date = timezone.now()

        tickets = Ticket.objects.filter(
            paid_token__isnull=False,
            created_at__range=(start_date, end_date))
        # colocar data do sorteio no filtro pra buscar
        for t in tickets:
            print('send ' + t.paid_token)
            st = self.check_transaction(t.paid_token)
            if st != t.status_paid:
                t.status_paid = st
                t.save()
            print(t.code, ' ', t.is_paid)

    def handle(self, *args, **options):
        self.data_payment()
