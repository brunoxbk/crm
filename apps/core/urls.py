from django.urls import path
from apps.core import views

app_name = 'core'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('mirror/generate/', views.MirrorFormView.as_view(), name='generate'),
]
