"""forms core."""
from django import forms
from datetime import datetime
from apps.archives.models import Code
from django.utils.translation import ugettext_lazy as _


class FormCustomLogin(forms.Form):
    """form login."""

    username = forms.CharField(
        label=_('Username'), max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(
        label=_('Password'), max_length=80, required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class FormMirror(forms.Form):
    """form mirror."""

    code1 = forms.ModelChoiceField(
        label=_('Code'),
        queryset=Code.objects.all(), required=True,
        widget=forms.Select(attrs={
            'class': 'form-control show-tick', 'required': True, }))
    box1 = forms.CharField(
        label=_('Box'), max_length=20, required=True,
        widget=forms.TextInput(attrs={
            'required': True, 'class': 'form-control'}))
    year1 = forms.CharField(
        label=_('Year'), required=True,
        initial=datetime.now().year, max_length=30,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': True, }))

    code2 = forms.ModelChoiceField(
        label=_('Code'),
        queryset=Code.objects.all(), required=True,
        widget=forms.Select(attrs={
            'class': 'form-control show-tick', 'required': True, }))
    box2 = forms.CharField(
        label=_('Box'), max_length=20, required=True,
        widget=forms.TextInput(attrs={
            'required': True, 'class': 'form-control'}))
    year2 = forms.CharField(
        label=_('Year'), required=True,
        initial=datetime.now().year, max_length=30,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': True, }))

    # code3 = forms.ModelChoiceField(
    #     label=_('Code'),
    #     queryset=Code.objects.all(), required=True,
    #     widget=forms.Select(attrs={
    #         'class': 'form-control show-tick', 'required': True, }))
    # box3 = forms.CharField(
    #     label=_('Box'), max_length=20, required=True,
    #     widget=forms.TextInput(attrs={
    #         'required': True, 'class': 'form-control'}))
    # year3 = forms.CharField(
    #     label=_('Year'), required=True,
    #     initial=datetime.now().year, max_length=30,
    #     widget=forms.TextInput(attrs={
    #         'class': 'form-control',
    #         'required': True, }))

    # code4 = forms.ModelChoiceField(
    #     label=_('Code'),
    #     queryset=Code.objects.all(), required=True,
    #     widget=forms.Select(attrs={
    #         'class': 'form-control show-tick', 'required': True, }))
    # box4 = forms.CharField(
    #     label=_('Box'), max_length=20, required=True,
    #     widget=forms.TextInput(attrs={
    #         'required': True, 'class': 'form-control'}))
    # year4 = forms.CharField(
    #     label=_('Year'), required=True,
    #     initial=datetime.now().year, max_length=30,
    #     widget=forms.TextInput(attrs={
    #         'class': 'form-control',
    #         'required': True, }))

    class Meta:
        fieldsets = (
          ('1', {
            'fields': ('code1', 'box1', 'year1')
          }),
          ('2', {
            'fields': ('code2', 'box2', 'year2')
          }),
          # ('3', {
          #   'fields': ('code3', 'box3', 'year3')
          # }),
          # ('4', {
          #   'fields': ('code4', 'box4', 'year4')
          # })
        )
