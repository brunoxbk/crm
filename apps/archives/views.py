from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from apps.archives.forms import FormArchive, FormSearch
from apps.archives.models import Archive
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
import datetime
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import user_passes_test
from django.utils.translation import activate


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)

class LoginRequiredMixin2(object):
    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, request, *args, **kwargs):
        activate('pt-br')
        return super(LoginRequiredMixin2, self)\
            .dispatch(request, *args, **kwargs)


class List(LoginRequiredMixin, ListView):
    template_name = "archives/list.html"
    model = Archive
    context_object_name = 'archives'
    paginate_by = 25

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('search', None):
            query.add(
                Q(code__code__icontains=self.request.GET['search']) |
                Q(code__title__icontains=self.request.GET['search']) |
                Q(description__icontains=self.request.GET['search']) |
                Q(keywords__icontains=self.request.GET['search']), Q.AND)

        if self.request.GET.get('year', None):
            query.add(Q(year=self.request.GET['year']), Q.AND)

        return Archive.objects.filter(query).order_by('-created_at')

    def get_context_data(self, **kwargs):
        context = super(List, self).get_context_data(**kwargs)
        context.update(dict(form_search=FormSearch(self.request.GET)))
        return context


class New(LoginRequiredMixin, FormView):
    template_name = 'archives/form.html'
    form_class = FormArchive
    success_url = reverse_lazy('archives:list')

    def form_valid(self, form):
        data = form.cleaned_data

        archive = Archive(
            code=data['code'],
            box=data['box'],
            shelf=data['shelf'],
            stand=data['stand'],
            row=data['row'],
            keywords=data['keywords'],
            year=data['year'],
            description=data['description'],
            creator=self.request.user,
            file=data['file'],
            dispatcher=data['dispatcher']
        )

        archive.save()

        msg = "%s %s %s" % (
            _("Archive"), archive.code, _("added successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(New, self).form_valid(form)


class Edit(LoginRequiredMixin, FormView):
    template_name = "archives/form.html"
    form_class = FormArchive
    success_url = reverse_lazy('archives:list')

    def get_object(self):
        return get_object_or_404(Archive, id=self.kwargs.get('pk'))

    def get_initial(self):
        self.initial = model_to_dict(self.get_object())
        return self.initial.copy()

    def form_valid(self, form):
        data = form.cleaned_data
        archive = self.get_object()

        archive.code = data['code']
        archive.box = data['box']
        archive.shelf = data['shelf']
        archive.row = data['row']
        archive.stand = data['stand']
        archive.year = data['year']
        archive.keywords = data['keywords']
        archive.description = data['description']
        archive.dispatcher = data['dispatcher']

        if data['file']:
            archive.file = data['file']

        archive.save()

        msg = "%s %s %s" % (
            _("Archive"), archive.code, _("updated successfully!"))
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(Edit, self).form_valid(form)


class Detail(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Archive
    context_object_name = 'archive'
    template_name = "archives/detail.html"


class Delete(LoginRequiredMixin2, DeleteView):
    model = Archive
    success_url = reverse_lazy('archives:list')
    template_name = 'archives/delete.html'
    success_message = _('Archive deleted successfully')
    slug_field = 'pk'
