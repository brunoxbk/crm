from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from apps.archives.validity import VALIDITY


class Code(models.Model):
    title = models.CharField(
        _("Title Code"), max_length=255, null=False, blank=False)
    code = models.CharField(
        _('Code'), max_length=55, blank=False, null=False)
    validity = models.IntegerField(
        _('Validity'), default=0, blank=False, null=False,
        help_text=_('In years, 0 to permanent.'))

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    status = models.BooleanField(_("Status"), default=True)

    class Meta:
        verbose_name = _("Archive")
        verbose_name_plural = _("Archive")
        get_latest_by = 'created_at'
        ordering = ('-created_at',)

    def __str__(self):
        return "%s %s" % (self.code, self.title)


class Archive(models.Model):
    code = models.ForeignKey(
        'archives.Code', blank=True, null=False, verbose_name=_("Code"),
        on_delete=models.DO_NOTHING, related_name='code_archive')
    year = models.CharField(
        _('Year'), max_length=10, blank=True, null=True)
    box = models.CharField(
        _("Number Box"), max_length=255, null=True, blank=True)
    shelf = models.CharField(
        _("Number Shelf"), max_length=255, null=False, blank=False)
    row = models.CharField(
        _("Number Row"), max_length=255, null=False, blank=False)
    stand = models.CharField(
        _("Stand"), max_length=255, null=True, blank=True)
    dispatcher = models.CharField(
        _("Dispatcher"), max_length=255, null=True, blank=True)
    keywords = models.CharField(
        _("Keywords"), max_length=255, null=True, blank=True)
    description = models.TextField(_("Description"), null=True, blank=True)

    file = models.FileField(
        _("Archive"), upload_to='documents/', null=True, blank=True)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    status = models.BooleanField(_("Status"), default=True)

    creator = models.ForeignKey(
        'users.User', blank=False, null=False, verbose_name=_("User"),
        on_delete=models.DO_NOTHING, related_name='user_archive')

    class Meta:
        verbose_name = _("Archive")
        verbose_name_plural = _("Archive")
        get_latest_by = 'created_at'
        ordering = ('-created_at',)

    def __str__(self):
        return "%s %s %s" % (_("Archive"), self.code.title, self.year)

    def validity_info(self):
        if VALIDITY[self.code]:
            return "%d anos" % VALIDITY[self.code]
        elif VALIDITY[self.code] is None:
            return "Permanente"

    def validity_date(self):
        if self.code.validity != 0:
            return int(self.year) + self.code.validity
        else:
            return "∞"

    def validity_str(self):
        if self.code.validity != 0:
            return int(self.year) + self.code.validity
        else:
            return "∞"

    def is_invalid(self):
        if self.code.validity == 0:
            return False
        else:
            return self.validity_date() < datetime.now().year

    def location(self):
        return "%s: %s | %s: %s | %s: %s <br> %s: %s | %s: %s" %  \
            (
                _("Box"), self.box, _("Shelf"), self.shelf,
                _("Row"), self.row, _("Stand"), self.stand,
                _("Year"), self.year,
            )

    @property
    def has_file(self):
        return self.file is not None
