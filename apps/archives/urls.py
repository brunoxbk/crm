from django.urls import path
from apps.archives import views

app_name = 'archives'

urlpatterns = [
    path('', views.List.as_view(), name='list'),
    path('new', views.New.as_view(), name='new'),
    path('edit/<int:pk>/', views.Edit.as_view(), name='edit'),
    path('detail/<int:pk>/', views.Detail.as_view(), name='detail'),
    path('delete/<int:pk>/', views.Delete.as_view(), name='delete'),
]
