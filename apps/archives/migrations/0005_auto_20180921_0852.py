# Generated by Django 2.0.4 on 2018-09-21 11:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archives', '0004_archive_stand'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archive',
            name='box',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Number Box'),
        ),
        migrations.AlterField(
            model_name='archive',
            name='year',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Year'),
        ),
    ]
