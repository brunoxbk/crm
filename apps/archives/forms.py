from django import forms
from django.conf import settings
from datetime import datetime
from apps.archives.models import Archive, Code
from django.utils.translation import ugettext_lazy as _
import re
from django.core.exceptions import ValidationError
from apps.archives.models import Archive


class FormSearch(forms.Form):
    search = forms.CharField(
        label=_('Search'), max_length=120, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    year = forms.CharField(
        label=_('Year'), required=False,  max_length=4,
        widget=forms.TextInput(attrs={'class': 'form-control'}))


class FormArchive(forms.Form):
    reference = forms.CharField(
        label=_('Reference Archive'), max_length=120, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    dispatcher = forms.CharField(
        label=_('Dispatcher'), max_length=120, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    keywords = forms.CharField(
        label=_('Keywords'), max_length=120, required=False,
        help_text=_('Separated by comma.'),
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'autocomplete': 'off'}
            )
        )
    code = forms.ModelChoiceField(
        label=_('Code'),
        queryset=Code.objects.all(), required=True,
        widget=forms.Select(attrs={
            'class': 'form-control show-tick',
            'required': True, 'autocomplete': 'off'
        })
    )
    year = forms.CharField(
        label=_('Year'), required=False, initial=datetime.now().year,
        max_length=4,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'autocomplete': 'off'}
        )
    )
    is_stand = forms.BooleanField(
        label=_('Stand'), required=False, initial=False,
        widget=forms.CheckboxInput(
            attrs={
                'required': False,
                'class': 'filled-in chk-col-indigo'
                }))
    box = forms.CharField(
        label=_('Box'), max_length=120,
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'autocomplete': 'off'}
        )
    )
    stand = forms.CharField(
        label=_('Stand'), max_length=120, required=False,
        widget=forms.TextInput(
            attrs={'required': False, 'class': 'form-control'})
        )
    shelf = forms.CharField(
        label=_('Shelf'), max_length=120, required=False, initial='',
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'autocomplete': 'off'}
        )
    )
    row = forms.CharField(
        label=_('Row'), max_length=120, required=True,
        widget=forms.TextInput(
            attrs={
                'required': True,
                'class': 'form-control',
                'autocomplete': 'off'
            })
        )
    description = forms.CharField(
        label=_('Description'), required=False,
        widget=forms.Textarea(
            attrs={'required': False, 'class': 'form-control'}
        )
    )
    file = forms.FileField(
        label=_("Archive"), required=False,
        widget=forms.ClearableFileInput(
            attrs={'required': False, 'class': 'form-control'}
        )
    )

    class Meta:
        fieldsets = (
          (None, {
            'fields': ('keywords', 'year')
          }),
          (None, {
            'fields': ('is_stand',)
          }),
          (None, {
            'fields': ('stand', 'shelf', 'row')
          }),
          (None, {
            'fields': ('code', 'box')
          }),
          (None, {
            'fields': ('description',)
          }),
          (None, {
            'fields': ('file',)
          }),
          (None, {
            'fields': ('dispatcher', )
          }),
        )
