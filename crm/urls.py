from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from apps.core import urls as core_urls
from apps.users import urls as users_urls
from apps.archives import urls as archives_urls
from apps.groups import urls as groups_urls
from apps.codes import urls as codes_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'users/', include(users_urls)),
    path(r'archives/', include(archives_urls)),
    path(r'groups/', include(groups_urls)),
    path(r'codes/', include(codes_urls)),
    path(r'', include(core_urls)),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT)
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
